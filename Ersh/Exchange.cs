﻿using System.Collections.Generic;

namespace Ersh
{
    /// <summary>
    /// Exchange simulator for one symbol
    /// </summary>
    public class Exchange
    {
        private int nextOrderId = 1;

        public string Symbol { get; set; }

        private Dictionary<string, Order> ActiveOrders = new Dictionary<string, Order>();

        public string SetOrder(NewOrderRequest order)
        {
            if(order.Symbol != Symbol)
                throw new ErshException($"Symbol {order.Symbol} is not supported by exchange, expected {Symbol}");

            var id = GetNextId();

            ActiveOrders[id] = order;
            return id;
        }

        private string GetNextId()
        {
            return Symbol + "_" + nextOrderId++;
        }

        public void CancelOrder(string id)
        {

        }
    }

    public class OrderBook
    {
        Dictionary<decimal, Order> levels = new Dictionary<decimal, Order>();

        public void AddOrder(Order order)
        {
            levels.Add(order.Price, order);
        }
    }

    public class OrderBookSide
    {
        public void MatchOrder(Order order)
        {

        }
    }

    public enum OrderState
    {
        PendingActive, Active, PendingCancel, Canceled, Filled
    }

    public enum OrderType
    {
        Limit, Market
    }

    public enum OrderSide
    {
        Buy, Sell
    }

    public class NewOrderRequest
    {
        public decimal Price { get; set; }
        public decimal Qty { get; set; }
        public OrderType Type { get; set; } = OrderType.Limit;

        public string Symbol { get; set; }
    }

    public class Order : NewOrderRequest
    {
        // client id
        public string Id { get; set; }
        // id on exchange
        public string ExternalId { get; set; }
        public decimal FilledQty { get; set; }
        public OrderState State { get; set; }
    }

    public class Trade
    {
        public string Id { get; set; }
        public string OrderId { get; set; }

        public decimal Price { get; set; }
        public decimal Qty { get; set; }
        public string Symbol { get; set; }
    }
}
